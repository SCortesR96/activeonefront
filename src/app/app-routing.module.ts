import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
   {
      path: '',
      pathMatch: 'full',
      redirectTo: 'activeone/categorias',
   },
   {
      path: 'activeone',
      loadChildren: () =>
         import('./modules/activeone/activeone.module').then(
            (m) => m.ActiveoneModule
         ),
   },
];

@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule],
})
export class AppRoutingModule {}
