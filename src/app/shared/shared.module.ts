import { ActiveoneRoutingModule } from './../modules/activeone/activeone-routing.module';
import { MatTableModule } from '@angular/material/table';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

// Material Modules
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
   declarations: [FooterComponent, NavbarComponent],
   imports: [
      CommonModule,
      MatTableModule,
      RouterModule,
      ActiveoneRoutingModule,
      ReactiveFormsModule,
      MatButtonModule,
      MatInputModule,
      MatSelectModule,
      MatDialogModule,
      MatIconModule,
   ],
   exports: [
      FooterComponent,
      NavbarComponent,
      ActiveoneRoutingModule,
      ReactiveFormsModule,
      MatButtonModule,
      MatInputModule,
      MatSelectModule,
      MatDialogModule,
      MatIconModule,
      NgxPaginationModule,
   ],
})
export class SharedModule {}
