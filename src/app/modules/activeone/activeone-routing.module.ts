import { CategoriasComponent } from './categorias/categorias.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostComponent } from './post/post.component';
import { ComentarioComponent } from './comentario/comentario.component';

const routes: Routes = [
   {
      path: 'categorias',
      component: CategoriasComponent,
   },
   {
      path: 'posts',
      component: PostComponent,
   },
   {
      path: 'comentarios',
      component: ComentarioComponent,
   },
];

@NgModule({
   imports: [RouterModule.forChild(routes)],
   exports: [RouterModule],
})
export class ActiveoneRoutingModule {}
