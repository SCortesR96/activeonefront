import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriasComponent } from './categorias/categorias.component';
import { FormComponent } from './categorias/form/form.component';
import { FormComponent as PostFormComponent } from './post/form/form.component';
import { FormComponent as ComentarioFormComponent } from './comentario/form/form.component';
import { PostComponent } from './post/post.component';
import { ComentarioComponent } from './comentario/comentario.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
   declarations: [
      CategoriasComponent,
      FormComponent,
      PostFormComponent,
      ComentarioFormComponent,
      PostComponent,
      ComentarioComponent,
   ],
   imports: [CommonModule, SharedModule],
})
export class ActiveoneModule {}
