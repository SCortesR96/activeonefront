import { IResponse } from '@interfaces/response';
import { Component, OnInit } from '@angular/core';
import { PostService } from '@core/services/post.service';
import { IPost } from '@interfaces/post';
import { MatDialog } from '@angular/material/dialog';
import { FormComponent } from './form/form.component';
import Swal from 'sweetalert2';

@Component({
   selector: 'app-post',
   templateUrl: './post.component.html',
   styleUrls: ['./post.component.css'],
})
export class PostComponent implements OnInit {
   page: number = 1;
   tamanoTabl: number = 5;
   posts: IPost[] = [];
   post: IPost = {
      id: 0,
      titulo: '',
      contenido: '',
      categorias_id: 0,
      estado: 1,
   };

   constructor(
      private _postService: PostService,
      public _matDialog: MatDialog
   ) {}

   ngOnInit(): void {
      this.listarPosts();
   }

   openDialog(data?: IPost) {
      const mostarDialog = this._matDialog.open(FormComponent, {
         width: '40vw',
         data,
      });

      mostarDialog.afterClosed().subscribe((resp) => {
         this.listarPosts();
      });
   }

   listarPosts() {
      this._postService.listarPosts().subscribe((resp: IResponse) => {
         this.posts = resp.data.posts;
      });
   }

   consultarPosts(id: number) {
      this._postService.consultarPost(id).subscribe((resp: IResponse) => {
         this.post = resp.data.post;
      });
   }

   eliminarPost(id: number) {
      this._postService.eliminarPost(id).subscribe((resp: any) => {
         Swal.fire({
            title: '¿Desea eliminar este post?',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: () => !Swal.isLoading(),
         }).then((result) => {
            if (result.isConfirmed) {
               if (resp) {
                  if (resp.success == true) {
                     Swal.fire('¡Información!', resp.message, 'success').then(
                        (resultado) => {
                           this._matDialog.closeAll();
                        }
                     );
                  } else {
                     Swal.fire('¡Información!', resp.message, 'error').then(
                        (resultado) => {
                           this._matDialog.closeAll();
                        }
                     );
                  }
               } else {
                  Swal.fire(
                     '¡Información!',
                     'Hubo un error en los datos, favor verificar',
                     'error'
                  ).then((resultado) => {
                     this._matDialog.closeAll();
                  });
               }
            }
            this.listarPosts();
         });
      });
   }
}
