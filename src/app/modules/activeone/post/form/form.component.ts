import { PostService } from '@core/services/post.service';
import { PostComponent } from './../post.component';
import { Component, Inject, OnInit } from '@angular/core';
import {
   FormBuilder,
   FormControl,
   FormGroup,
   Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IPost } from '@interfaces/post';
import Swal from 'sweetalert2';
import { CategoriaService } from '@core/services/categoria.service';

@Component({
   selector: 'app-form',
   templateUrl: './form.component.html',
   styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
   // Inicializar Variables
   postForm: FormGroup;
   categoriaLista: Array<any> = [];

   constructor(
      private fb: FormBuilder,
      public _matDialog: MatDialogRef<PostComponent>,
      private _postService: PostService,
      private _categoriaServicio: CategoriaService,
      @Inject(MAT_DIALOG_DATA) public data: IPost
   ) {
      this.postForm = this.fb.group({
         id: new FormControl(0),
         titulo: new FormControl('', Validators.required),
         contenido: new FormControl('', Validators.required),
         categorias_id: new FormControl(0, Validators.required),
         estado: new FormControl(null, [Validators.min(0), Validators.max(1)]),
         fecha_creacion: new FormControl(),
      });
   }

   ngOnInit(): void {
      console.log(this.data);
      if (this.data.id > 0) {
         this.postForm.setValue(this.data);

         this.postForm.controls['estado'].setValidators([Validators.required]);
      }

      this.listarCategorías();
   }

   crearPost() {
      this._postService
         .crearPost(this.postForm.value)
         .subscribe((resp: any) => {
            if (resp) {
               if (resp.success == true) {
                  Swal.fire('¡Información!', resp.message, 'success').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               } else {
                  Swal.fire('¡Información!', resp.message, 'error').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               }
            } else {
               Swal.fire(
                  '¡Información!',
                  'Hubo un error en los datos, favor verificar',
                  'error'
               ).then((resultado) => {
                  this._matDialog.close();
               });
            }
         });
   }

   actualizarPost() {
      this._postService
         .actualizarPost(this.data.id, this.postForm.value)
         .subscribe((resp: any) => {
            if (resp) {
               if (resp.success == true) {
                  Swal.fire('¡Información!', resp.message, 'success').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               } else {
                  Swal.fire('¡Información!', resp.message, 'error').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               }
            } else {
               Swal.fire(
                  '¡Información!',
                  'Hubo un error en los datos, favor verificar',
                  'error'
               ).then((resultado) => {
                  this._matDialog.close();
               });
            }
         });
   }

   listarCategorías() {
      this._categoriaServicio.listarCategorias().subscribe(({ data }) => {
         this.categoriaLista = data.categorias;
         console.log(this.categoriaLista);
      });
   }
}
