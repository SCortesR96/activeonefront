import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ICategoria } from '@interfaces/categoria';
import { IResponse } from '@interfaces/response';
import { CategoriaService } from 'src/app/core/services/categoria.service';
import Swal from 'sweetalert2';
import { FormComponent } from './form/form.component';

@Component({
   selector: 'app-categorias',
   templateUrl: './categorias.component.html',
   styleUrls: ['./categorias.component.css'],
})
export class CategoriasComponent implements OnInit {
   page: number = 1;
   tamanoTabl: number = 5;

   categorias: ICategoria[] = [];
   categoria: ICategoria = {
      id: 0,
      nombre: '',
      estado: 1,
   };

   constructor(
      private _categoriaService: CategoriaService,
      public _matDialog: MatDialog
   ) {}

   ngOnInit(): void {
      this.listarCategorias();
   }

   openDialog(data?: ICategoria) {
      const mostarDialog = this._matDialog.open(FormComponent, {
         width: '40vw',
         data,
      });

      mostarDialog.afterClosed().subscribe((resp) => {
         this.listarCategorias();
      });
   }

   listarCategorias() {
      this._categoriaService.listarCategorias().subscribe((resp: IResponse) => {
         this.categorias = resp.data.categorias;
      });
   }

   consultarCategorias() {
      this._categoriaService
         .consultarCategoria(1)
         .subscribe((resp: IResponse) => {
            this.categorias = resp.data.categorias;
         });
   }

   eliminarCategoria(id: number) {
      this._categoriaService.eliminarCategoria(id).subscribe((resp: any) => {
         Swal.fire({
            title: '¿Desea eliminar esta categoría?',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: () => !Swal.isLoading(),
         }).then((result) => {
            if (result.isConfirmed) {
               if (resp) {
                  if (resp.success == true) {
                     Swal.fire('¡Información!', resp.message, 'success').then(
                        (resultado) => {
                           this._matDialog.closeAll();
                        }
                     );
                  } else {
                     Swal.fire('¡Información!', resp.message, 'error').then(
                        (resultado) => {
                           this._matDialog.closeAll();
                        }
                     );
                  }
               } else {
                  Swal.fire(
                     '¡Información!',
                     'Hubo un error en los datos, favor verificar',
                     'error'
                  ).then((resultado) => {
                     this._matDialog.closeAll();
                  });
               }
            }
            this.listarCategorias();
         });
      });
   }
}
