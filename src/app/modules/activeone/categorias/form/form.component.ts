import { CategoriaService } from 'src/app/core/services/categoria.service';
import { CategoriasComponent } from './../categorias.component';
import { Component, Inject, OnInit } from '@angular/core';
import {
   FormBuilder,
   FormControl,
   FormGroup,
   Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICategoria } from '@interfaces/categoria';
import Swal from 'sweetalert2';

@Component({
   selector: 'app-form',
   templateUrl: './form.component.html',
   styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
   // Inicializar Variables
   categoriaForm: FormGroup;

   constructor(
      private fb: FormBuilder,
      public _matDialog: MatDialogRef<CategoriasComponent>,
      private _categoriaServicio: CategoriaService,
      @Inject(MAT_DIALOG_DATA) public data: ICategoria
   ) {
      this.categoriaForm = this.fb.group({
         id: new FormControl(0),
         nombre: new FormControl('', Validators.required),
         estado: new FormControl(null, [Validators.min(0), Validators.max(1)]),
         fecha_creacion: new FormControl(),
      });
   }

   ngOnInit(): void {
      if (this.data.id > 0) {
         this.categoriaForm.setValue(this.data);

         this.categoriaForm.controls['estado'].setValidators([
            Validators.required,
         ]);
      }
   }

   crearCategoria() {
      this._categoriaServicio
         .crearCategoria(this.categoriaForm.value)
         .subscribe((resp: any) => {
            if (resp) {
               if (resp.success == true) {
                  Swal.fire('¡Información!', resp.message, 'success').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               } else {
                  Swal.fire('¡Información!', resp.message, 'error').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               }
            } else {
               Swal.fire(
                  '¡Información!',
                  'Hubo un error en los datos, favor verificar',
                  'error'
               ).then((resultado) => {
                  this._matDialog.close();
               });
            }
         });
   }

   actualizarCategoria() {
      this._categoriaServicio
         .actualizarCategoria(this.data.id, this.categoriaForm.value)
         .subscribe((resp: any) => {
            if (resp) {
               if (resp.success == true) {
                  Swal.fire('¡Información!', resp.message, 'success').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               } else {
                  Swal.fire('¡Información!', resp.message, 'error').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               }
            } else {
               Swal.fire(
                  '¡Información!',
                  'Hubo un error en los datos, favor verificar',
                  'error'
               ).then((resultado) => {
                  this._matDialog.close();
               });
            }
         });
   }
}
