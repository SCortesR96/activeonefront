import { ComentarioComponent } from './../comentario.component';
import { Component, Inject, OnInit } from '@angular/core';
import {
   FormBuilder,
   FormControl,
   FormGroup,
   Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComentarioService } from '@core/services/comentario.service';
import { IComentario } from '@interfaces/comentario';
import Swal from 'sweetalert2';
import { PostService } from '@core/services/post.service';

@Component({
   selector: 'app-form',
   templateUrl: './form.component.html',
   styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
   // Inicializar Variables
   comentarioForm: FormGroup;
   postLista: Array<any> = [];

   constructor(
      private fb: FormBuilder,
      public _matDialog: MatDialogRef<ComentarioComponent>,
      private _comentarioServicio: ComentarioService,
      private _postService: PostService,
      @Inject(MAT_DIALOG_DATA) public data: IComentario
   ) {
      this.comentarioForm = this.fb.group({
         id: new FormControl(0),
         contenido: new FormControl('', Validators.required),
         posts_id: new FormControl(0, Validators.required),
         estado: new FormControl(null, [Validators.min(0), Validators.max(1)]),
         fecha_creacion: new FormControl(),
      });
   }

   ngOnInit(): void {
      if (this.data.id > 0) {
         this.comentarioForm.setValue(this.data);

         this.comentarioForm.controls['estado'].setValidators([
            Validators.required,
         ]);
      }

      this.listarPosts();
   }

   listarPosts() {
      this._postService.listarPosts().subscribe(({ data }) => {
         this.postLista = data.posts;
         console.log(this.postLista);
      });
   }

   crearComentario() {
      this._comentarioServicio
         .crearComentario(this.comentarioForm.value)
         .subscribe((resp: any) => {
            if (resp) {
               if (resp.success == true) {
                  Swal.fire('¡Información!', resp.message, 'success').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               } else {
                  Swal.fire('¡Información!', resp.message, 'error').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               }
            } else {
               Swal.fire(
                  '¡Información!',
                  'Hubo un error en los datos, favor verificar',
                  'error'
               ).then((resultado) => {
                  this._matDialog.close();
               });
            }
         });
   }

   actualizarComentario() {
      this._comentarioServicio
         .actualizarComentario(this.data.id, this.comentarioForm.value)
         .subscribe((resp: any) => {
            if (resp) {
               if (resp.success == true) {
                  Swal.fire('¡Información!', resp.message, 'success').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               } else {
                  Swal.fire('¡Información!', resp.message, 'error').then(
                     (resultado) => {
                        this._matDialog.close();
                     }
                  );
               }
            } else {
               Swal.fire(
                  '¡Información!',
                  'Hubo un error en los datos, favor verificar',
                  'error'
               ).then((resultado) => {
                  this._matDialog.close();
               });
            }
         });
   }
}
