import { IResponse } from '@interfaces/response';
import { Component, OnInit } from '@angular/core';
import { IComentario } from '@interfaces/comentario';
import { MatDialog } from '@angular/material/dialog';
import { FormComponent } from './form/form.component';
import { ComentarioService } from '@core/services/comentario.service';
import Swal from 'sweetalert2';

@Component({
   selector: 'app-comentario',
   templateUrl: './comentario.component.html',
   styleUrls: ['./comentario.component.css'],
})
export class ComentarioComponent implements OnInit {
   page: number = 1;
   tamanoTabl: number = 5;

   comentarios: IComentario[] = [];
   comentario: IComentario = {
      id: 0,
      contenido: '',
      posts_id: 0,
   };

   constructor(
      private _comentarioService: ComentarioService,
      public _matDialog: MatDialog
   ) {}

   ngOnInit(): void {
      this.listarComentarios();
   }

   openDialog(data?: IComentario) {
      const mostarDialog = this._matDialog.open(FormComponent, {
         width: '40vw',
         data,
      });

      mostarDialog.afterClosed().subscribe((resp) => {
         this.listarComentarios();
      });
   }

   listarComentarios() {
      this._comentarioService
         .listarComentarios()
         .subscribe((resp: IResponse) => {
            this.comentarios = resp.data.comentarios;
         });
   }

   eliminarComentario(id: number) {
      this._comentarioService.eliminarComentario(id).subscribe((resp: any) => {
         Swal.fire({
            title: '¿Desea eliminar este comentario?',
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: () => !Swal.isLoading(),
         }).then((result) => {
            if (result.isConfirmed) {
               if (resp) {
                  if (resp.success == true) {
                     Swal.fire('¡Información!', resp.message, 'success').then(
                        (resultado) => {
                           this._matDialog.closeAll();
                        }
                     );
                  } else {
                     Swal.fire('¡Información!', resp.message, 'error').then(
                        (resultado) => {
                           this._matDialog.closeAll();
                        }
                     );
                  }
               } else {
                  Swal.fire(
                     '¡Información!',
                     'Hubo un error en los datos, favor verificar',
                     'error'
                  ).then((resultado) => {
                     this._matDialog.closeAll();
                  });
               }
            }
            this.listarComentarios();
         });
      });
   }
}
