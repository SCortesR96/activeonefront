import { Endpoint } from './endpoints';
import { Injectable } from '@angular/core';

@Injectable({
   providedIn: 'root',
})
export class ComentarioSettingService {
   constructor() {}

   public comentarios = {
      url: {
         listar: Endpoint.urlBase('comentarios'),
      },
   };
}
