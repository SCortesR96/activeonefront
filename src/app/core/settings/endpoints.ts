import { environment } from 'src/environments/environment';

export class Endpoint {
   static urlBase(url: string): string {
      return environment.urlLocal + url;
      // return environment.urlApi + url;
   }
}
