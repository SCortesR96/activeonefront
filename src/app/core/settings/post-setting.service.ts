import { Endpoint } from './endpoints';
import { Injectable } from '@angular/core';

@Injectable({
   providedIn: 'root',
})
export class PostSettingService {
   constructor() {}

   public posts = {
      url: {
         listar: Endpoint.urlBase('posts'),
      },
   };
}
