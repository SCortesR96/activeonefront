import { Endpoint } from './endpoints';
import { Injectable } from '@angular/core';

@Injectable({
   providedIn: 'root',
})
export class CategoriaSettingService {
   constructor() {}

   public categorias = {
      url: {
         listar: Endpoint.urlBase('categorias'),
      },
   };
}
