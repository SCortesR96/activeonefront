import { IResponse } from '@interfaces/response';
import { ComentarioSettingService } from './../settings/comentario-setting.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
   providedIn: 'root',
})
export class ComentarioService {
   constructor(
      private _httpClient: HttpClient,
      private _ccomentarioSetting: ComentarioSettingService
   ) {}

   listarComentarios() {
      return this._httpClient.get<IResponse>(
         this._ccomentarioSetting.comentarios.url.listar
      );
   }

   consultarComentario(comentario: number) {
      return this._httpClient.get(
         `${this._ccomentarioSetting.comentarios.url.listar}/${comentario}`
      );
   }

   crearComentario(data: object) {
      return this._httpClient.post(
         this._ccomentarioSetting.comentarios.url.listar,
         data
      );
   }

   actualizarComentario(comentario: number, data: object) {
      return this._httpClient.put(
         `${this._ccomentarioSetting.comentarios.url.listar}/${comentario}`,
         data
      );
   }

   eliminarComentario(comentario: number) {
      return this._httpClient.post(
         `${this._ccomentarioSetting.comentarios.url.listar}/${comentario}`,
         null
      );
   }
}
