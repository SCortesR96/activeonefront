import { CategoriaSettingService } from './../settings/categoria-setting.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IResponse } from '@interfaces/response';

@Injectable({
   providedIn: 'root',
})
export class CategoriaService {
   constructor(
      private _httpClient: HttpClient,
      private _categoriaSetting: CategoriaSettingService
   ) {}

   listarCategorias() {
      return this._httpClient.get<IResponse>(
         this._categoriaSetting.categorias.url.listar
      );
   }

   consultarCategoria(categoria: number) {
      return this._httpClient.get<IResponse>(
         `${this._categoriaSetting.categorias.url.listar}/${categoria}`
      );
   }

   crearCategoria(data: object) {
      return this._httpClient.post(
         this._categoriaSetting.categorias.url.listar,
         data
      );
   }

   actualizarCategoria(categoria: number, data: object) {
      return this._httpClient.put(
         `${this._categoriaSetting.categorias.url.listar}/${categoria}`,
         data
      );
   }

   eliminarCategoria(categoria: number) {
      return this._httpClient.post(
         `${this._categoriaSetting.categorias.url.listar}/${categoria}`,
         null
      );
   }
}
