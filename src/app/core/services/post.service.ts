import { PostSettingService } from './../settings/post-setting.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResponse } from '@interfaces/response';

@Injectable({
   providedIn: 'root',
})
export class PostService {
   constructor(
      private _httpClient: HttpClient,
      private _postSetting: PostSettingService
   ) {}

   listarPosts() {
      return this._httpClient.get<IResponse>(this._postSetting.posts.url.listar);
   }

   consultarPost(post: number) {
      return this._httpClient.get<IResponse>(
         `${this._postSetting.posts.url.listar}/${post}`
      );
   }

   crearPost(data: object) {
      return this._httpClient.post(this._postSetting.posts.url.listar, data);
   }

   actualizarPost(post: number, data: object) {
      return this._httpClient.put(
         `${this._postSetting.posts.url.listar}/${post}`,
         data
      );
   }

   eliminarPost(post: number) {
      return this._httpClient.post(
         `${this._postSetting.posts.url.listar}/${post}`,
         null
      );
   }
}
