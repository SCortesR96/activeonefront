export interface IComentario {
   id: number;
   contenido: string;
   posts_id: number;
   estado?: 0 | 1;
   anulado?: 0 | 1;
   fecha_creacion?: Date;
   fecha_actualizacion?: Date;
}
