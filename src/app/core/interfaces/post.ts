export interface IPost {
   id: number;
   titulo: string;
   contenido: string;
   categorias_id: number;
   estado?: 0 | 1;
   anulado?: 0 | 1;
   fecha_creacion?: Date;
   fecha_actualizacion?: Date;
}
