export interface ICategoria {
   id: number;
   nombre: string;
   estado?: 0 | 1;
   anulado?: 0 | 1;
   fecha_creacion?: Date;
   fecha_actualizacion?: Date;
}
