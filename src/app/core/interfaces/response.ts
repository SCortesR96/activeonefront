import { IComentario } from './comentario';
import { IPost } from './post';
import { ICategoria } from './categoria';

export interface IResponse {
   success: boolean;
   message: string;
   code: number;
   data: any;
}
